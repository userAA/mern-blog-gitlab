import express from 'express';
import multer from 'multer';
import cors from 'cors';

import mongoose from 'mongoose';
import { 
        //функция проверки данных по регистрации пользователя
	registerValidation, 
        //функция проверки данных по авторизации пользователя
	loginValidation, 
        //функция проверки данных для созданию статьи авторизованным пользователем
	articleCreateValidation 
} from './validations.js';

import {
	handleValidationErrors, 
 	//функция проверки существования токена авторизованного пользователя
	checkAuth
} from './utils/index.js';
import {
	//контроллер который отвечает за операции с пользовательскими данными
	UserController, 
	//котроллер который отвечает за операции со статьями
	ArticleController
} from './controllers/index.js';

//подключаем базу данных Mongo DB
mongoose.connect
(
    "mongodb://127.0.0.1:27017/mern-blog",
    {
        useNewUrlParser: true,
        useUnifiedTolology: true,
        useCreateIndex: true
    }
)
.then(() => 
{
    console.log("Succesfully connected to MongoDB");
})
.catch ((error) => 
{
    console.log("Unable to connect to MongoDB!");
    console.error(error);    
})

const app = express();

//механизм сохранения изображения в статье в отдельной папке
const storage = multer.diskStorage({
    destination: (_, __, cb) => {
        cb(null, 'uploads');
    },
    filename: (_, file, cb) => {
        cb(null, file.originalname)
    }
})

const upload = multer({storage});

//гарантирует ответ с запросов в json формате
app.use(express.json());
//гарантирует перенос информации между серверами с различными локальными адресами 
app.use(cors());
app.use('/uploads',express.static('uploads'));

//маршрутизатор авторизации пользователя
app.post('/auth/login', loginValidation, handleValidationErrors, UserController.login);
//маршрутизатор регистрации пользователя
app.post('/auth/register', registerValidation, handleValidationErrors, UserController.register);
//маршрутизатор получения данных по авторизованному пользователю, если зафиксирован токен авторизованного пользователя
app.get('/auth/me', checkAuth, UserController.getMe);

app.post('/upload', checkAuth, upload.single('image'), (req, res) => {
    res.json({
        url: `/uploads/${req.file.originalname}`
    })
});

//маршрутизатор получения последних тэгов из базы данных ArticleModel по последним пяти статьям
app.get('/tags',  ArticleController.getLastTags);
//маршрутизатор получения всех статей из базы данных ArticleModel
app.get('/articles', ArticleController.getAll);

//маршрутизатор получения одной статьи с идентификатором articleId
app.get('/articles/:id', ArticleController.getOne);
//маршрутизатор получения одной статьи с идентификатором articleId
app.post('/articles', checkAuth, articleCreateValidation, handleValidationErrors, ArticleController.create);
//маршрутизатор удаления статьи
app.delete('/articles/:id', checkAuth, ArticleController.remove);
//маршрутизатор редактирования статьи
app.patch('/articles/:id',checkAuth, articleCreateValidation, handleValidationErrors, ArticleController.update);

app.listen(3001, (err) => {
    if (err) { 
        return console.log(err);
    }
    console.log('Server OK');
})