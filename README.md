gitlab page https://gitlab.com/userAA/mern-blog-gitlab.git
gitlab comment mern-blog-gitlab

проект mern-blog (простой блог на redux toolkit)
используемые технологии на фронтенде:
    @emotion/react,
    @emotion/styled,
    @mui/icons-material,
    @mui/material,
    @reduxjs/toolkit,
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    clsx,
    easymde,
    prettier,
    react,
    react-dom,
    react-hook-form,
    react-markdown,
    react-redux,
    react-router-dom,
    react-scripts,
    react-simplemde-editor,
    sass,
    web-vitals;

используемые технологии на бекэнде:
    bcrypt,
    cors,
    express,
    express-validator,
    jsonwebtoken,
    mongoose,
    multer;

Проводится регистрация пользователя и авторизация пользователя. Выводится список статей по всем 
пользователям в базе данных. По авторизованному пользователю можно создавать новую статью. Статьи 
авторизованного пользователя можно добавлять или удалять. 

